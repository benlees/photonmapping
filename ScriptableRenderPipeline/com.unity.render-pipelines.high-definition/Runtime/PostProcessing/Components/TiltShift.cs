using System;

namespace UnityEngine.Rendering.HighDefinition
{
    [Serializable, VolumeComponentMenu("Post-processing/Tilt Shift")]
    public sealed class TiltShift : VolumeComponent, IPostProcessComponent
    {
        [Tooltip("")]
        public ClampedFloatParameter blurFactor = new ClampedFloatParameter(1.5f, 0f, 10f);

        [Tooltip("")]
        public ClampedFloatParameter blurPower = new ClampedFloatParameter(3f, 1f, 10f);

        [Tooltip("")]
        public ClampedFloatParameter blurSlope = new ClampedFloatParameter(2.5f, 0.1f, 4f);

        [Tooltip("")]
        public ClampedFloatParameter blurOffset = new ClampedFloatParameter(0.25f, 0.1f, 1f);

        [Tooltip("")]
        public ClampedIntParameter blurSteps = new ClampedIntParameter(5, 3, 15);

        public bool IsActive()
        {
            return blurFactor.value > 0f && blurPower.value >= 1f;
        }
    }
}
