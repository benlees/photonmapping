﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.Rendering;
using Unity.Mathematics;

public class CausticMap : MonoBehaviour
{
    [SerializeField] ComputeShader m_Shader;            //Caustic map shader
    [SerializeField] ComputeShader m_BlurShader;        //Gaussian blur shader
    [SerializeField] int m_TextureSize = 256;           //Resolution of the caustic map
    [SerializeField] int m_SamplingGroupSize = 1024;    //Number of photons per pass = 1024 * m_SamplingGroupSize.  1024 is the compute shader's group size.
    [SerializeField] MeshRenderer m_Receiver;           //Plane mesh to project the caustic map on to.  This must be a unity plane primitive.
    [SerializeField] Light m_PointLight;                //The point light to cast against all spheres.
    [SerializeField] float m_ClearModifier = 0.2f;      //The photon map is cleared every frame.  Clear value of a pixel = m_ClearModifier * currentPixelValue.  This allows accumulation to occur across multiple frames.
    [SerializeField] float m_PhotonEnergy = 0.03f;      //A photon incident on the photon map will add (m_PhotonEnergy * inverse distance * m_PointLight.intensity) to a pixel.
    [SerializeField] int m_NumPasses = 1;               //Number of m_Shader.Caustic passes written by each Refractor
    [SerializeField] int m_NumBlurPasses = 1;           //5 step gaussian blur applied after final photon mapping.  Applied recursively m_NumBlurPasses times.
    [SerializeField] bool m_RandomizeRaycast = true;    //This will rotate the sampling group along it's forward axis during a pass to simulate noise.  Disable for deterministic behavior.
    [SerializeField] Material m_OverrideMaterial;       //Material used to draw into the gBuffer.
    [SerializeField] bool m_UseAsyncCompute = true;     //Executes the command buffer asynchronously.
    bool m_UseUniformSampling = false;              //Generates uniform sampling pattern for positions. Can sometimes increase tex hit rate (experimental, proof of concept only).

    //Compute shader kernels
    int m_CausticKernel;
    int m_ClearKernel;
    int m_BlurHorizontalKernel;
    int m_BlurVerticalKernel;
    int m_ClearThreadGroupSize;

    //All objects with a Refractor component.  All of these object contribute to photon mapping.
    //These objects must be sphere primitives with uniform scale.
    Refractor[] m_Refractors;

    //Render targets
    RenderTexture m_CausticMap;
    RenderTexture m_IntermediateBlurTexture;

    //Instance of the m_OverrideMaterial.  Properties of this material may be edited by script at runtime.
    Material m_OverrideMaterialInstance;

    //Accessors
    public MeshRenderer receiverPlane { get { return m_Receiver; } }
    public new Light light { get { return m_PointLight; } }
    public Material overrideMaterial { get { return m_OverrideMaterialInstance; } }

    //Compute buffer and RenderTargetIdentifiers
    ComputeBuffer m_SamplingComputeBuffer;
    RenderTargetIdentifier m_CausticMapRTI;
    RenderTargetIdentifier m_IntermediateBlurRTI;
    RenderTargetIdentifier[] m_DebugMapRTI;

    //Shader property IDs
    int m_TransmissionColorID = Shader.PropertyToID("_TransmissionColor");
    int m_RefractorPositionID = Shader.PropertyToID("_RefractorPosition");
    int m_RefractorRadiusID = Shader.PropertyToID("_RefractorRadius");
    int m_ReceiverWorldToUnitSpaceMatrixID = Shader.PropertyToID("_ReceiverWorldToUnitSpaceMatrix");
    int m_TextureParamsID = Shader.PropertyToID("_TextureParams");
    int m_IndexOfRefractionID = Shader.PropertyToID("_IndexOfRefraction");
    int m_ReceiverPositionID = Shader.PropertyToID("_ReceiverPosition");
    int m_ReceiverNormalID = Shader.PropertyToID("_ReceiverNormal");
    int m_PhotonEnergyID = Shader.PropertyToID("_PhotonEnergy");
    int m_LightPositionID = Shader.PropertyToID("_LightPosition");
    int m_SamplingLocalToWorldMatrixID = Shader.PropertyToID("_SamplingLocalToWorldMatrix");
    int m_CausticMapID = Shader.PropertyToID("_CausticMap");
    int m_ClearModifierID = Shader.PropertyToID("_ClearModifier");
    int m_SamplingMapID = Shader.PropertyToID("_SamplingMap");
    int m_InputID = Shader.PropertyToID("_Input");
    int m_OutputID = Shader.PropertyToID("_Output");
    int m_SamplingBufferID = Shader.PropertyToID("_SamplingBuffer");
    int m_SamplingBufferOffsetID = Shader.PropertyToID("_SamplingBufferOffset");
    int m_CausticConstantBufferID = Shader.PropertyToID("_CausticConstantBuffer");

    private void Awake()
    {
        //Gather all refractors that will contribute to the photon map.
        m_Refractors = GameObject.FindObjectsOfType<Refractor>();
    }

    void Start()
    {
        //Get all compute shader kernels
        m_CausticKernel = m_Shader.FindKernel("Caustic");
        m_ClearKernel = m_Shader.FindKernel("Clear");
        m_BlurHorizontalKernel = m_BlurShader.FindKernel("BlurHorizontal");
        m_BlurVerticalKernel = m_BlurShader.FindKernel("BlurVertical");

        //Create sampling buffer and render textures
        CreateSamplingBuffer();
        m_CausticMap = CreateIOTexture(m_TextureSize, RenderTextureFormat.ARGBFloat);
        m_IntermediateBlurTexture = CreateIOTexture(m_TextureSize, RenderTextureFormat.ARGBFloat);

        //Generate RenderTargetIdentifiers
        m_CausticMapRTI = new RenderTargetIdentifier(m_CausticMap);
        m_IntermediateBlurRTI = new RenderTargetIdentifier(m_IntermediateBlurTexture);

        //Get a persistent material instance for use later.
        m_OverrideMaterialInstance = new Material(m_OverrideMaterial);
        m_OverrideMaterialInstance.SetTexture(m_CausticMapID, m_CausticMap);

        m_ClearThreadGroupSize = m_TextureSize / 32;     //TODO: query directly from compute shader.  This is only used for the m_ClearKernel
    }

    private void OnDestroy()
    {
        //Allocated a very large compute buffer.  Make sure to delete it.
        if(m_SamplingComputeBuffer.IsValid())
        {
            m_SamplingComputeBuffer.Dispose();
        }
    }

    //Setup a render texture with the appropriate properties.
    RenderTexture CreateIOTexture(int textureSize, RenderTextureFormat format = RenderTextureFormat.ARGBFloat, FilterMode filterMode = FilterMode.Bilinear)
    {
        RenderTexture rt = new RenderTexture(textureSize, textureSize, 0, format, 0);
        rt.enableRandomWrite = true;
        rt.format = format;
        rt.wrapMode = TextureWrapMode.Repeat;
        rt.filterMode = filterMode;
        rt.useMipMap = false;
        rt.Create();
        return rt;
    }

    //Generate random sampling points on the 'up' hemisphere of the unit sphere.
    //The 1D compute buffer contains a list of random points described by the photon distribution function
    //and aperture of each refractor.  These are combined into a single compute buffer to avoid the cost
    //of repeatedly switching smaller compute buffers between passes.  Each refractor stores it's offset
    //into this array.
    //TODO: Allow refractors to use the same compute buffer position.
    void CreateSamplingBuffer()
    {
        uint xThreads, yThreads, zThreads;
        m_Shader.GetKernelThreadGroupSizes(m_CausticKernel, out xThreads, out yThreads, out zThreads);
        Vector3[] samplingSourceBuffer = new Vector3[m_SamplingGroupSize * (int)xThreads * m_Refractors.Length];

        int refractorIdx = 0;
        foreach(Refractor refractor in m_Refractors)
        {
            int subBufferSize = m_SamplingGroupSize * (int)xThreads;

            if (m_UseUniformSampling)
            {
                refractor.WriteUniformPositionsToSamplingBuffer(ref samplingSourceBuffer, subBufferSize, subBufferSize * refractorIdx);
            }
            else
            {
                refractor.WritePositionsToSamplingBuffer(ref samplingSourceBuffer, subBufferSize, subBufferSize * refractorIdx);
            }
            refractorIdx++;
        }

        m_SamplingComputeBuffer = new ComputeBuffer(samplingSourceBuffer.Length, sizeof(float) * 3, ComputeBufferType.Structured, ComputeBufferMode.Immutable);
        m_SamplingComputeBuffer.SetData(samplingSourceBuffer);
        m_Shader.SetBuffer(m_CausticKernel, m_SamplingBufferID, m_SamplingComputeBuffer);
    }

    //Adds the full caustic path into the passed command buffer.
    public void Draw(ScriptableRenderContext context, CommandBuffer cmd)
    {
        if (m_UseAsyncCompute)
        {
            ExecuteAsyncCompute(context, cmd);
        }
        else
        {
            CreateClearCommandBuffer(cmd);
            CreateCausticsCommandBuffer(cmd);
            CreateBlurCommandBuffer(cmd);
        }
    }

    void ExecuteAsyncCompute(ScriptableRenderContext context, CommandBuffer cmd)
    {
        CreateClearCommandBuffer(cmd);
        context.ExecuteCommandBuffer(cmd);
        cmd.Clear();

        cmd.SetExecutionFlags(CommandBufferExecutionFlags.AsyncCompute);

        CreateCausticsCommandBuffer(cmd);
        context.ExecuteCommandBufferAsync(cmd, ComputeQueueType.Default);
        cmd.Clear();

        cmd.SetExecutionFlags(CommandBufferExecutionFlags.None);
        CreateBlurCommandBuffer(cmd);
        context.ExecuteCommandBuffer(cmd);
        cmd.Clear();
    }

    //Add the clear segment to a command buffer.
    void CreateClearCommandBuffer(CommandBuffer cmd)
    {
        cmd.SetComputeTextureParam(m_Shader, m_ClearKernel, m_CausticMapID, m_CausticMapRTI);
        cmd.SetComputeFloatParam(m_Shader, m_ClearModifierID, Mathf.Clamp01(m_ClearModifier));
        cmd.DispatchCompute(m_Shader, m_ClearKernel, m_ClearThreadGroupSize, m_ClearThreadGroupSize, 1);
    }

    //Add a gaussian blur pass to the command buffer.
    void CreateBlurCommandBuffer(CommandBuffer cmd)
    {
        for (int i = 0; i < m_NumBlurPasses; i++)
        {
            //Apply horizontal blur
            cmd.SetComputeTextureParam(m_BlurShader, m_BlurHorizontalKernel, m_InputID, m_CausticMapRTI);
            cmd.SetComputeTextureParam(m_BlurShader, m_BlurHorizontalKernel, m_OutputID, m_IntermediateBlurRTI);
            cmd.DispatchCompute(m_BlurShader, m_BlurHorizontalKernel, m_ClearThreadGroupSize, m_ClearThreadGroupSize, 1);

            //Apply vertical blur
            cmd.SetComputeTextureParam(m_BlurShader, m_BlurVerticalKernel, m_InputID, m_IntermediateBlurRTI);
            cmd.SetComputeTextureParam(m_BlurShader, m_BlurVerticalKernel, m_OutputID, m_CausticMapRTI);
            cmd.DispatchCompute(m_BlurShader, m_BlurVerticalKernel, m_ClearThreadGroupSize, m_ClearThreadGroupSize, 1);
        }
    }

    //Adds the caustic passes to a command buffer.
    void CreateCausticsCommandBuffer(CommandBuffer cmd)
    {
        foreach (Refractor refractor in m_Refractors)
        {
            //Calculate fake photon energy proportional to light intensity, num passes, and squared distance.
            float photonEnergy = m_PhotonEnergy * m_PointLight.intensity / m_NumPasses / (m_PointLight.transform.position - refractor.transform.position).sqrMagnitude;

            //TODO: Remove static values.
            cmd.SetComputeTextureParam(m_Shader, m_CausticKernel, m_CausticMapID, m_CausticMapRTI);
            cmd.SetComputeVectorParam(m_Shader, m_RefractorPositionID, refractor.transform.position);
            cmd.SetComputeVectorParam(m_Shader, m_TransmissionColorID, refractor.GetTransmissionColorAsVector4(m_PointLight));// new Vector4(m_TransmissionColor.r, m_TransmissionColor.g, m_TransmissionColor.b, 0.0f));
            cmd.SetComputeFloatParam(m_Shader, m_RefractorRadiusID, 0.5f * refractor.transform.localScale.x);
            cmd.SetComputeMatrixParam(m_Shader, m_ReceiverWorldToUnitSpaceMatrixID, GetReceiverWorldToUnitSpaceMatrix());
            cmd.SetComputeVectorParam(m_Shader, m_TextureParamsID, new Vector4(m_TextureSize, m_TextureSize, 1.0f / m_TextureSize, 1.0f / m_TextureSize));
            cmd.SetComputeFloatParam(m_Shader, m_IndexOfRefractionID, refractor.indexOfRefraction);
            cmd.SetComputeVectorParam(m_Shader, m_ReceiverPositionID, m_Receiver.transform.position);
            cmd.SetComputeVectorParam(m_Shader, m_ReceiverNormalID, m_Receiver.transform.up);
            cmd.SetComputeFloatParam(m_Shader, m_PhotonEnergyID, photonEnergy);
            cmd.SetComputeIntParam(m_Shader, m_SamplingBufferOffsetID, refractor.bufferOffset);

            //Generate a new rotation matrix for each pass.
            //JitterAxis is combined with a refractor's spread to apply fake view- and light-dependent distortion to the caustic pattern.
            float rotationStepPerPass = 360.0f / m_NumPasses;
            Vector3 jitterAxis = m_PointLight.transform.position - m_Receiver.transform.position.normalized;
            for (int i = 0; i < m_NumPasses; i++)
            {
                cmd.SetComputeVectorParam(m_Shader, m_LightPositionID, m_PointLight.transform.position + refractor.sourceSpread * (Quaternion.AngleAxis(i * rotationStepPerPass, jitterAxis) * Vector3.right));
                cmd.SetComputeMatrixParam(m_Shader, m_SamplingLocalToWorldMatrixID, GetSamplingLocalToWorldMatrix(refractor.transform, m_RandomizeRaycast ? UnityEngine.Random.value * 360f : i * rotationStepPerPass));
                cmd.DispatchCompute(m_Shader, m_CausticKernel, m_SamplingGroupSize, 1, 1);
            }
        }
    }

    //Transforms the sampling hemisphere to point at the light.  Additionally, the hemisphere
    //is rotated along it's forward axis to create 'random' noise (if applicable).
    Matrix4x4 GetSamplingLocalToWorldMatrix(Transform refractorTransform, float angularRotation)
    {
        Vector3 sphereToLightDirection = (m_PointLight.transform.position - refractorTransform.position).normalized;
        
        Quaternion worldFacingRot = Quaternion.FromToRotation(Vector3.up, sphereToLightDirection);
        Quaternion axisRotation = Quaternion.AngleAxis(angularRotation, sphereToLightDirection);
        Matrix4x4 worldFacingMatrix = Matrix4x4.TRS(Vector3.zero, axisRotation * worldFacingRot, Vector3.one);

        Matrix4x4 finalLocalToWorld = refractorTransform.localToWorldMatrix * worldFacingMatrix;
        
        return finalLocalToWorld;
    }

    //Modifies the worldToLocalMatrix to account for the local bounds of the receiver
    //such that a unit vector multiplied by this matrix will be in the unit sphere.
    //This final vector can then be adjusted (translated and scaled) to map to texture
    //coordinates.
    Matrix4x4 GetReceiverWorldToUnitSpaceMatrix()
    {
        MeshRenderer meshRenderer = m_Receiver.GetComponent<MeshRenderer>();
        Vector3 boundsModifier = meshRenderer.bounds.extents;
        
        return m_Receiver.worldToLocalMatrix * Matrix4x4.Scale(new Vector3(1.0f / boundsModifier.x, 1.0f, 1.0f / boundsModifier.z));
    }
    
    private void Update()
    {
#if UNITY_EDITOR
        if(!Validate())
        {
            return;
        }
#endif
    }

#if UNITY_EDITOR
    bool Validate()
    {
        if (m_PointLight == null || m_Receiver == null || m_Refractors == null || m_OverrideMaterial == null)
        {
            Debug.LogWarning("The caustic map's point light, receiver, refractor, or material are not set.");
            return false;
        }

        Vector3 lossyScale;

        foreach (Refractor refractor in m_Refractors)
        {
            lossyScale = refractor.transform.lossyScale;
            if (!Mathf.Approximately(lossyScale.x, lossyScale.y) || !Mathf.Approximately(lossyScale.x, lossyScale.z))
            {
                Debug.LogWarning(refractor.gameObject.name + " does not have uniform scale.");
                return false;
            }
        }

        lossyScale = m_Receiver.transform.lossyScale;
        if (!Mathf.Approximately(lossyScale.x, lossyScale.y) || !Mathf.Approximately(lossyScale.x, lossyScale.z))
        {
            Debug.LogWarning("The receiver does not have uniform scale.");
            return false;
        }
        return true;
    }

    //void DebugDrawSamples()
    //{
    //    if(m_DebugMap == null)
    //    {
    //        return;
    //    }
    //    Matrix4x4 localToWorld = GetSamplingLocalToWorldMatrix(0);

    //    Texture2D[] tempTextures = new Texture2D[2];

    //    tempTextures[0] = new Texture2D(m_TextureSize, m_TextureSize, TextureFormat.RGBAFloat, false);
    //    tempTextures[1] = new Texture2D(m_TextureSize, m_TextureSize, TextureFormat.RGBAFloat, false);
    //    RenderTexture previous = RenderTexture.active;
    //    RenderTexture.active = m_DebugMap[0];
    //    tempTextures[0].ReadPixels(new Rect(0, 0, tempTextures[0].width, tempTextures[0].height), 0, 0);
    //    tempTextures[0].Apply();
    //    RenderTexture.active = m_DebugMap[1];
    //    tempTextures[1].ReadPixels(new Rect(0, 0, tempTextures[1].width, tempTextures[1].height), 0, 0);
    //    tempTextures[1].Apply();
    //    RenderTexture.active = previous;

    //    for (int x = 0; x < tempTextures[0].width && x < 16; x++)
    //    {
    //        for (int y = 0; y < tempTextures[0].height && y < 16; y++)
    //        {
    //            Color localPos = sourceSamplingTexture.GetPixel(x, y);
    //            Vector3 worldPos = new Vector3(localPos.r, localPos.g, localPos.b);
    //            worldPos = localToWorld.MultiplyPoint3x4(worldPos);

    //            Color debugColorOne = tempTextures[0].GetPixel(x, y);
    //            Vector3 debugPosOne = new Vector3(debugColorOne.r, debugColorOne.g, debugColorOne.b);

    //            Color debugColorTwo = tempTextures[1].GetPixel(x, y);
    //            Vector3 debugPosTwo = new Vector3(debugColorTwo.r, debugColorTwo.g, debugColorTwo.b);

    //            Debug.DrawLine(worldPos, debugPosOne, debugColorOne.a > 0f ? (debugColorOne.a < 2f ? Color.white : Color.yellow) : Color.red);
    //            Debug.DrawLine(debugPosTwo, debugPosOne, debugColorTwo.a > 0f ? (debugColorOne.a < 2f ? Color.white : Color.yellow) : Color.red);
    //        }
    //    }
    //    Destroy(tempTextures[0]);
    //    Destroy(tempTextures[1]);
    //}
#endif
}
