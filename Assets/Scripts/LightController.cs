﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class LightController : MonoBehaviour
{
    [SerializeField] float m_RestitutionHeight = 1f;
    [SerializeField] float m_RestitutionForce = 1f;
    [SerializeField] float m_GravityRate = 20f;
    [SerializeField] float m_GravityForce = 5f;
    [SerializeField] float m_AngularForce = 1f;
    [SerializeField] float m_PlanarForce = 1f;
    [SerializeField] bool m_MotionOnStart = true;

    Rigidbody m_RigidBody;
    bool running = true;

    void Awake()
    {
        m_RigidBody = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        running = m_MotionOnStart;
        Time.timeScale = running ? 1.0f : 0.0f;
    }

    private void Update()
    {
        //Pause/restart the simulation if the user presses Space key
        if(Input.GetKeyDown(KeyCode.Space))
        {
            running = !running;
            Time.timeScale = running ? 1.0f : 0.0f;
        }
        if(Input.GetKeyDown(KeyCode.Q) || Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    void FixedUpdate()
    {
        if (running)
        {
            Vector3 force = Vector3.zero;

            //Apply a vertical force if the light is below a threshold distance.  
            //Proportionally larger as the distance from threshold increases.
            float yDiff = m_RigidBody.position.y - m_RestitutionHeight;
            if (yDiff < 0f)
            {
                force += Vector3.up * m_RestitutionForce * Mathf.Abs(yDiff);
            }

            Vector3 radialDir = transform.position.normalized;

            //Apply an orbital force that always points orthogonal to the radial direction.
            Vector3 orbitDir = Vector3.Cross(radialDir, Vector3.up);
            force += orbitDir * m_AngularForce;

            //Apply a radial force constrained to the xy plane.
            Vector3 planarDir = Vector3.ProjectOnPlane(radialDir, Vector3.up);
            force += m_PlanarForce * planarDir;

            //Apply a varying force along the vertical axis.
            force += m_GravityForce * Mathf.Sin(Time.time * m_GravityRate) * Vector3.down;

            //Apply all combined forces.
            m_RigidBody.AddForce(force * Time.fixedDeltaTime, ForceMode.Force);
            
        }

    }
}
