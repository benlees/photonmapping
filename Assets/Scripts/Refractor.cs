﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Refractor : MonoBehaviour
{
    static float s_LocalSphereRadius = 0.5f;        //Source model's local sphere radius.  Used for computing positions on the local sphere surface.

    [SerializeField] AnimationCurve m_PhotonDistribution;           //Distribution of photons along the aperture.  Adjust to vary the appearance of spherical caustics.
    [SerializeField] float m_ApertureAngle = 12f;                   //Photons outside of this half-angle will be culled.
    [SerializeField] bool m_ApproximateMinimalAperture = true;      //Calculates an approximate cone-angle for a unit sized sphere.  This will override m_ApertureAngle
    [SerializeField] float m_IndexOfRefraction = 3f;                //Used for refraction calculations.  This value should be unrealistically large because this simulation uses primary refractions only.
    [SerializeField] float m_SourceSpread = 0f;                     //World space offset from the light source applied to each pass.  Increases the 'volume' of the point light source.
    [SerializeField] float m_TransmissionSaturationModifier = 1f;   //The color of final photons is faked as (light color * refractor color).  The saturation of this color is multiplied by m_TransmissionSaturationModifier.
    [SerializeField] int m_BufferOffset;

    MeshRenderer m_MeshRenderer;
    
    //Accessors
    public float indexOfRefraction { get { return m_IndexOfRefraction; } }
    public float sourceSpread { get { return m_SourceSpread; } }
    public int bufferOffset { get { return m_BufferOffset; } }
    public float coneAngle { get { return GetConeAngle(); } }

    private void Awake()
    {
        m_MeshRenderer = GetComponent<MeshRenderer>();
    }

    //Gets a local space position of a unit sphere based on this sphere's m_PhotonDistribution and halfAngle (derived from m_ApertureAngle)
    Vector3 GetUnitSpherePositionInsideCone(float halfAngle)
    {
        float xAngle = m_PhotonDistribution.Evaluate(UnityEngine.Random.value) * (UnityEngine.Random.value < 0.5f ? 1.0f : -1.0f) * halfAngle;
        float yAngle = UnityEngine.Random.Range(0f, 360.0f);

        return Quaternion.Euler(xAngle, yAngle, 0.0f) * Vector3.up;
    }

    float GetConeAngle()
    {
        float angle = m_ApertureAngle;
        if (m_ApproximateMinimalAperture)
        {
            //A maximum of two refractions occur for each photon.  Photons incident with the sphere above the maxRefraction angle can be discarded.
            //Photons exiting the sphere above the critical angle can be discarded.  The critical angle is linearly proportional to the maxRefraction
            //angle - specifically, the derivative of maxRefraction is always orthogonal to critical angle.  Therefore, a regression of maxRefraction
            //to coneAngle is sufficient to approximate the spherical arc angle of acceptable photons.
            float eta = m_IndexOfRefraction < 1.0f ? 1.0f : m_IndexOfRefraction;
            float maxRefraction = Mathf.Asin(Mathf.Sqrt(1.0f - (1.0f / (eta * eta)))) * Mathf.Rad2Deg;
            float criticalAngle = Mathf.Asin(1.0f / m_IndexOfRefraction) * Mathf.Rad2Deg;
            angle = 5E-05f * maxRefraction * maxRefraction * maxRefraction + 0.0016f * maxRefraction * maxRefraction - 1.531f * maxRefraction + 88.241f;
        }
        return angle;
    }

    //Writes local space positions into buffer.  These positions occur at a coneAngle on the sphere
    //described by m_ApertureAngle and are distributed in this region according to the m_PhotonDistribution.
    //This buffer is uploaded to a structured buffer and applied during the caustic map generation.
    public void WritePositionsToSamplingBuffer(ref Vector3[] buffer, int length, int startIndex)
    {
        m_BufferOffset = startIndex;
        
        for(int i = startIndex; i < length + startIndex; i++)
        {
            buffer[i] = GetUnitSpherePositionInsideCone(coneAngle) * s_LocalSphereRadius;
        }
    }

    //Proof of concept for comparison of nearly-spaced positions vs. random distribution of positions.
    //Used in compute to compare tex cache hits when computing positions.
    public void WriteUniformPositionsToSamplingBuffer(ref Vector3[] buffer, int length, int startIndex)
    {
        m_BufferOffset = startIndex;

        float pointsPerArc = 100f;
        float pointsPerCircle = 400f;
        float dx = 1.0f / pointsPerArc;// coneAngle / pointsPerArc;
        float dy = 360f / pointsPerCircle;
        float x = 0.0f;
        float y = 0.0f;
        float angle = coneAngle;
        
        for (int i = startIndex; i < length + startIndex; i++)
        {
            x += Random.Range(0.5f, 1f) * dx;
            if (x > 1.0f)
            {
                x -= 1.0f;
                y += Random.Range(0.5f, 1f) * dy;
            }
            if (y >= 360.0f)
            {
                y -= 360.0f;
            }

            float xDist = m_PhotonDistribution.Evaluate(x) * angle;

            buffer[i] = Quaternion.Euler(xDist, y, 0.0f) * Vector3.up * s_LocalSphereRadius;
        }
    }

    //Returns the product of light and material.color.  The saturation of this value can be
    //adjusted by m_TransmissionSaturationModifier.
    public Vector4 GetTransmissionColorAsVector4(Light light)
    {
        float h, s, v;
        Color refractorColor = m_MeshRenderer.material.GetColor("_TransmittanceColor");
        Color.RGBToHSV(refractorColor, out h, out s, out v);

        s = Mathf.Clamp01(s * m_TransmissionSaturationModifier);

        Color transmissionColor = light.color * Color.HSVToRGB(h, s, v);
        return new Vector4(transmissionColor.r, transmissionColor.g, transmissionColor.b, 1.0f);
    }
}
