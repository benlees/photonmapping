﻿Shader "Custom/LightLayerShader" {
	Properties{ }

		Category{
			Tags { }
			Blend One One
			ColorMask RGB
			Cull Off Lighting Off ZWrite Off
			ZTest Always

			SubShader {
				Pass {

					CGPROGRAM
					#pragma vertex vert
					#pragma fragment frag
					#pragma target 2.0

					#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Common.hlsl"
					#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Packing.hlsl"

					sampler2D _CausticMap;
					UNITY_DECLARE_TEX2DARRAY(_NormalTexture);
					float4 _CausticLightPositionWS;

					struct appdata_t {
						float4 vertex : POSITION;
						float2 texcoord : TEXCOORD0;
					};

					struct v2f {
						float4 vertex : SV_POSITION;
						float2 texcoord : TEXCOORD0;
						float4 screenPos : TEXCOORD1;
						float3 worldPos : TEXCOORD2;
					};

					//From UnityCG.cginc
					inline float4 ComputeNonStereoScreenPos(float4 pos) {
						float4 o = pos * 0.5f;
						o.xy = float2(o.x, o.y*_ProjectionParams.x) + o.w;
						o.zw = pos.zw;
						return o;
					}

					v2f vert(appdata_t v)
					{
						v2f o;
						o.vertex = UnityObjectToClipPos(v.vertex);
						o.texcoord = v.texcoord;
						o.screenPos = ComputeNonStereoScreenPos(o.vertex);
						o.worldPos = mul(unity_ObjectToWorld, v.vertex);
						return o;
					}

					//From NormalBuffer.hlsl
					float3 DecodeFromNormalBuffer(float4 normalBuffer)
					{
						float3 packNormalWS = normalBuffer.rgb;
						float2 octNormalWS = Unpack888ToFloat2(packNormalWS);
						float3 normalWS = UnpackNormalOctQuadEncode(octNormalWS * 2.0 - 1.0);
						return normalWS;
					}

					fixed4 frag(v2f i) : SV_Target
					{
						//Sample the caustic map
						fixed4 col = tex2D(_CausticMap, i.texcoord);

						//Sample and decode the gbuffer in screen space
						float3 screenCoord = float3(i.screenPos.xy / i.screenPos.w, 0);
						float4 normalSample = UNITY_SAMPLE_TEX2DARRAY(_NormalTexture, screenCoord);
						float3 normal = DecodeFromNormalBuffer(normalSample);

						//Modify intensity using the light's position to approximate incoming light direction.
						//A physically correct model would do this calculation per photon during the gather stage instead.
						float3 lightDir = normalize(_CausticLightPositionWS.xyz - i.worldPos.xyz);
						float lightFactor = saturate(dot(normal, lightDir));

						return float4(col.rgb * lightFactor, 1);
					}
					ENDCG
				}
			}
		}
}
